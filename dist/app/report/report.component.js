'use strict';

// Define the `report` component that populates index.html via the report Directive
contentReportApp.component('report', {
  templateUrl: 'dist/app/report/report.component.html',
  controller: ReportController
});